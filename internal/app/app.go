package app

import (
	"context"
	"net"

	"gitlab.com/mongosh/test_crud/config"
	"gitlab.com/mongosh/test_crud/genproto/test"
	"gitlab.com/mongosh/test_crud/internal/controller/service"
	"gitlab.com/mongosh/test_crud/pkg/db"
	"gitlab.com/mongosh/test_crud/pkg/logger"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func Run(cfg *config.Config) {
	l := logger.New(cfg.LogLevel, "test-crud")
	defer logger.CleanUp(l)

	connDB, err := db.ConnectToDb(cfg)
	if err != nil {
		l.Fatal("Error while to connection with database", logger.Error(err))
	}
	defer connDB.Disconnect(context.Background())

	testServices := service.NewTestService(connDB, l)

	lis, err := net.Listen("tcp", ":"+cfg.TestServicePort)
	if err != nil {
		l.Fatal("Error while to connection with TCP PROTOCOL", logger.Error(err))
	}

	c := grpc.NewServer()
	reflection.Register(c)
	test.RegisterTestServiceServer(c, testServices)
	l.Info(`Server is Running`,
		logger.String("port", cfg.TestServicePort))
	if err := c.Serve(lis); err != nil {
		l.Fatal("Error while listening with TCP/UDP", logger.Error(err))
	}

}
