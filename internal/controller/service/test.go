package service

import (
	"context"

	"gitlab.com/mongosh/test_crud/genproto/test"
	"gitlab.com/mongosh/test_crud/pkg/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (t *TestService) Created(ctx context.Context, req *test.TestInfo) (*test.TestInfo, error) {
	
	responce, err := t.Storage.Test().Created(req)
	if err != nil {
		t.Logger.Error(`Error while creating Test-MONGODB`, logger.Any(`Error with Test`, err))
		return &test.TestInfo{}, status.Error(codes.Internal, `Could not create you test info`)
	}
	return responce, nil
}

func (t *TestService) CreatedTest(ctx context.Context, req *test.TestInfo) (*test.TestInfo, error) {

	responce, err := t.Storage.Test().CreatedTest(req)
	if err != nil {
		t.Logger.Error(`Error while CREATED`, logger.Any(`Error with Connect to Test`, err))
		return &test.TestInfo{}, status.Error(codes.Internal, `NOT`)
	}
	
	return responce, nil
}