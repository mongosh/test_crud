package service

import (
	"gitlab.com/mongosh/test_crud/internal/controller/storage"
	"gitlab.com/mongosh/test_crud/pkg/logger"
	"go.mongodb.org/mongo-driver/mongo"
)

type TestService struct {
	Storage storage.IStorageI
	Logger logger.Logger
}

func NewTestService(db *mongo.Client, log logger.Logger) *TestService {
	return &TestService{
		Storage: storage.NewStoragePg(db),
		Logger: log,
	}
}