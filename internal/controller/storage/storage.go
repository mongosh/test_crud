package storage

import (
	"gitlab.com/mongosh/test_crud/internal/controller/storage/repo"
	"go.mongodb.org/mongo-driver/mongo"
	m"gitlab.com/mongosh/test_crud/internal/controller/storage/mongo"
)

type IStorageI interface{
	Test() repo.TestStorageI
}

type StoragePg struct {
	Db *mongo.Client
	testRepo	repo.TestStorageI
}

func NewStoragePg(db *mongo.Client) *StoragePg {
	return &StoragePg{
		Db: db,
		testRepo: m.NewTestRepo(db),
	}
}

func (s StoragePg) Test() repo.TestStorageI {
	return s.testRepo
}