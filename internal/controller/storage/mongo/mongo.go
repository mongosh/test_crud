package mongo

import "go.mongodb.org/mongo-driver/mongo"

type TestRepo struct {
	Db *mongo.Client
}

func NewTestRepo(db *mongo.Client) *TestRepo {
	return &TestRepo{Db: db}
}