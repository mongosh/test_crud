package mongo

import (
	"context"
	"time"

	"github.com/google/uuid"
	"gitlab.com/mongosh/test_crud/genproto/test"
	"go.mongodb.org/mongo-driver/bson"
)

func (t *TestRepo) Created(req *test.TestInfo) (*test.TestInfo, error) {

	collection := t.Db.Database("testdb").Collection("test")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	
	req.Id = uuid.New().String()
	_, err := collection.InsertOne(ctx, bson.D{
		{Key: "id", Value: req.Id},
		{Key: "user_name", Value: req.UserName},
		{Key: "email", Value: req.Email},
		{Key: "phone", Value: req.Phone},
	})
	if err != nil {
		return &test.TestInfo{}, err
	}

	return req, nil
}

func (t *TestRepo) CreatedTest(req *test.TestInfo) (*test.TestInfo, error) {
	
	collection := t.Db.Database("testdb").Collection("test")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	responce := &test.TestInfo{}
	responce.Id = uuid.New().String()
	_, err := collection.InsertOne(ctx, bson.D{
		{Key: "id", Value: responce.Id},
		{Key: "user_name", Value: &req.UserName},
		{Key: "email", Value: &req.Email},
		{Key: "phone", Value: &req.Phone},
	})
	if err != nil {
		return &test.TestInfo{}, err
	}

	responce.UserName = req.UserName
	responce.Email = req.Email
	responce.Phone = req.Phone
	
	return responce, nil
}