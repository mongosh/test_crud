package repo

import "gitlab.com/mongosh/test_crud/genproto/test"

type TestStorageI interface {
	Created(*test.TestInfo) (*test.TestInfo, error) 
	CreatedTest(*test.TestInfo) (*test.TestInfo, error)
}