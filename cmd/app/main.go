package main

import (
	"gitlab.com/mongosh/test_crud/config"
	"gitlab.com/mongosh/test_crud/internal/app"
)

func main() {
	cfg := config.LoadConfig()
	app.Run(cfg)
}